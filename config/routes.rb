require 'api_constraints'

MoviewPitcher::Application.routes.draw do

  resources :sessions, only: [:new, :create, :destroy]
  resources :registrations, only: [:new, :create, :create_via_fb]

  match '/location', to: 'index#theaters_near_location', :via => :get

  match '/load_more_movies',  to: 'index#load_more_movies'

  match '/signup',  to: 'registrations#signup'

  match '/settings',  to: 'index#settings'

  match '/account/email_preference',  to: 'index#do_save_account_email_preferences'

  match '/signin',  to: 'sessions#new'
  
  match '/fblogin', to: 'registrations#create_via_fb'
  
  match '/fbconnections', to: 'facebook#set_fb_connections', :via => :post

  match '/fbconnections', to: 'facebook#get_fb_connections', :via => :get

  match '/forgot_password', to: 'index#forgot_password', :via => :get

  match '/send_password_reset_email', to: 'index#send_password_reset_email', :via => :post

  match '/reset_password', to: 'index#reset_password', :via => :post

  match '/newpost',  to: 'post#new'

  match '/rating', to: 'review_rating#create', :via => :post

  
  match '/awaiting', to: 'post#get_awaitingness', :via => :get

  match '/admin', to: 'admin#admin'

  match '/movie/create', to: 'movie#create', :via => :post

  match '/critic/add', to: 'index#add_critic', :via => :post

  match '/critic/review_rating', to: 'index#add_critic_review_rating', :via => :post

  match '/watchlist/add', to: 'post#set_awaitingness', :via => :post
  
  match '/watchlist/remove', to: 'index#remove_from_watchlist', :via => :post

  match '/watchlist/watched', to: 'index#watched_a_movie_from_watchlist', :via => :post

  match '/signout', to: 'sessions#destroy', as: 'signout'

  # Mobile
  #match '/mobile/home', to: 'mobile_api#home', :via => :get

  match '/mobile', to: 'mobile_api#app_download_page', :via => :get

  match '/mobile/fblogin', to: 'mobile_api#fblogin', :via => :post

  match '/mobile/post_review', to: 'mobile_api#post_review', :via => :post

  #match '/mobile/reviews', to: 'mobile_api#movie_reviews', :via => :post

  match '/mobile/fbconnections', to: 'mobile_api#set_fb_connections', :via => :post 
  # Search
  match '/search/:type/:q', to: 'search#search', :via => :get

  match '/facebook', to: 'index#facebook', :via => :get


  root :to => 'index#home'

  match '/m/:movie_id', :to => 'index#movie_review', :via => :get

  match '/p/:people_id', :to => 'people#people_details', :via => :get

  match '/movie_suggestion', :to => 'index#movie_suggestion', :via => :get

  match '/send_email', :to => 'index#test_email', :via => :get 

  # APIs for Javascript
  match '/critic_review_ratings/:movie_id', :to => 'review_rating#critic_review_rating', :via => :get 
  
  match '/about', :to => 'index#about', :via => :get

        match '/showtimes', :to => 'showtimes#get_showtimes_for_movie_in_zone', :via => :get

        namespace :api, defaults: {format: 'json'} do
                scope module: :v1, constraints: ApiConstraints.new(version: 1) do
                        match 'home', to: 'mobile_api#home', :via => :get
                        match 'reviews', to: 'mobile_api#movie_reviews', :via => :get
                        match 'login', to: 'mobile_api#login', :via => :post     
                end
                scope module: :v2, constraints: ApiConstraints.new(version: 2, default: true) do
                        match 'home', to: 'mobile_api#home', :via => :get
                end
        end

end
