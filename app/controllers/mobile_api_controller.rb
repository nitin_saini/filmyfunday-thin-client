require "typhoeus"

class MobileApiController < ApplicationController
	
	def post_review
		rating = params[:rating]
		review_title = params[:review_title]
		review = params[:review]
		movie_id = params[:movie_id]
		user_id = params[:user_id]
		user_name  = params[:user_name]

		apiresponse = nil
		message = 'Not Okay'
		status = 0
		


		r = {
			:rating => rating
		}

		viewer_review = {
			:viewer_id => user_id,
			:viewer_name => user_name,
			:review_title => review_title,
			:review => review,
			:movie_id => movie_id,

		}

		p = {
			:viewer_review => viewer_review
		}
		
		status = 1 # Do sanity check

		if status == 1
			rating_post_response = Typhoeus::Request.post( REVIEW_RATING_SERVICE + "ratings/user/"+ user_id.to_s+"/movie/" + movie_id.to_s , :body => r)
			review_post_response = Typhoeus::Request.post( REVIEW_RATING_SERVICE + "reviews/viewer/create" , :body => p)
			if ((review_post_response.code >= 200 && review_post_response.code < 300) &&
				(rating_post_response.code >= 200 && rating_post_response.code < 300))
				rating_post_response_json =  JSON.parse(rating_post_response.body)
				review_post_response_json =  JSON.parse(review_post_response.body)

				message = 'Okay'

				if rating_post_response_json['status'] == 1
					status = 1 
				else
					status = 0
					message = 'Error in rating service'
				end
				
				if review_post_response_json['status'] == 1
					status = 1 
				else
					status = 0
					message = 'Error in review service'
				end
			else
				status = 0
				message = 'Review post :'+ review_post_response.code.to_s + ', Rating post :'+ rating_post_response.code.to_s
			end
		end



		apiresponse = {
			:status => status,
			:message => message
		}

		render json: apiresponse.to_json
	end

	def app_download_page
		if mobile_device?
			render 'layouts/mobile/app_download_web_page'
		else
			render 'layouts/app_download_web_page'
		end
	end

	def home

		hydra = Typhoeus::Hydra.new

		movie_id_list = nil

		list_of_movies_request = Typhoeus::Request.new( MOVIE_SERVICE + "movies.json?segregated=1")
		list_of_movies_request.on_complete do |list_of_movies_response|
			list_of_movies_response_json = JSON.parse(list_of_movies_response.body)
			if list_of_movies_response_json['status'] == 1
				@upcoming = list_of_movies_response_json['data']['upcoming']
				@released = list_of_movies_response_json['data']['released']
				movie_id_array = Array.new
				movie_array = Array.new

				@released.each do |nmr|
					movie_id_array << nmr['id']
					movie_array << nmr
					
				end

				arr_string = movie_id_array.join(",")

				movie_array_hash = movie_array.index_by{ |element| element['id'].to_s }

				ratings_for_movies_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "ratings/movie?movie_ids="+ arr_string)
				ratings_for_movies_request.on_complete do |ratings_for_movies_response|
					ratings_for_movies_response_json = JSON.parse(ratings_for_movies_response.body)
					if ratings_for_movies_response_json['status'] == 1
						movie_id_array.each do |m_id|
							movie = movie_array_hash[m_id.to_s]
							movie.delete 'created_at'
							movie.delete 'updated_at'
							movie['viewer_rating'] = ratings_for_movies_response_json['data'][m_id.to_s]['user']['rating'].to_f.round(2) 
							movie['critic_rating'] = ratings_for_movies_response_json['data'][m_id.to_s]['critic']['rating'].to_f.round(2) 
							if movie['viewer_rating'] == 0
								movie['viewer_rating'] = 0
							end
							if movie['critic_rating'] == 0
								movie['critic_rating'] = 0
							end
							logger.debug(movie)
						end
					end
				end		
				hydra.queue(ratings_for_movies_request)
				hydra.run		
			end	
		end
		hydra.queue(list_of_movies_request)
		hydra.run

		data = {
			:released => @released,
			:upcoming => @upcoming
		}

		apiresponse = {
			:status => 1,
			:data => data
		}

		render json: apiresponse.to_json
	end

	def fblogin
		#hydra = Typhoeus::Hydra.new
		status = 0
		message = 'Not okay'
		data = nil
		p = nil
		apiresponse = nil

		register_url = USER_SERVICE + "createupdatefb"

		if params[:fb_id] == nil || params[:fb_id].length == 0
			status = 0

		elsif params[:fname] == nil || params[:fname].length == 0
			status = 0
		elsif params[:lname] == nil || params[:lname].length == 0
			status = 0
		elsif params[:email] == nil || params[:email].length == 0
			status = 0
		else	
			status = 1
			user = {
				:fb_id => params[:fb_id],
				:fname => params[:fname],
				:lname => params[:lname],
				:email => params[:email],
			}
			p = {
				:user => user
			}
		end

		if status == 0
			message = 'Invalid parameters'
		end

		if status == 1
			# all is well, make a request
			register_user_request_response = Typhoeus::Request.post( register_url , :body => p )
			apiresponse = nil

			if register_user_request_response.code >= 200 && register_user_request_response.code  < 300
				register_user_request_response_json = JSON.parse(register_user_request_response.body)
				if register_user_request_response_json['status'] == 1
					status = 1
					message = 'User found'
					data = register_user_request_response_json['data']	
				end
				
			else
				status = 0
				message = register_user_request_response.code.to_s + ' encountered'
				data = nil
			end			
		end

		
		apiresponse = {
			:status => status,
			:message => message,
			:data => data
		}

		render json: apiresponse.to_json
	end

	def movie_reviews
		movie_id = params[:movie_id]
		user_id = params[:user_id]

		status = 0
		message = 'Not okay'
		data = nil

		review_ratings = nil

		apiresponse = nil
		
		hydra = Typhoeus::Hydra.new

		# critic review request
		if user_id != nil
			review_rating_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "ratings/movie?movie_ids="+movie_id+"&viewer_id="+user_id.to_s)
		else
			review_rating_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "ratings/movie?movie_ids="+movie_id)
		end
		
		
		review_rating_request.on_complete do |review_rating_response|
			if review_rating_response.code >= 200  && review_rating_response.code < 300
				review_rating_response_json = JSON.parse(review_rating_response.body)
				status = 1
				message = 'Okay'
				review_ratings = review_rating_response_json['data']
			else
				message = 'Service error '+review_rating_response.code.to_s
				review_ratings = nil
			end			
		end
		hydra.queue(review_rating_request)

		critic_review_ratings_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "critic_review_ratings.json", :body => {:movie_id => movie_id} )
		critic_review_ratings_request.on_complete do |critic_review_ratings_response|
			if critic_review_ratings_response.code >= 200 || critic_review_ratings_response.code < 300
				critic_review_ratings_response_json = JSON.parse(critic_review_ratings_response.body)

				if critic_review_ratings_response_json['status'] == 1
					@critic_reviews = critic_review_ratings_response_json['data']
				else
					@critic_reviews = nil
				end
			end

		end
		hydra.queue(critic_review_ratings_request)

		if user_id != nil
			friends_review_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE  + "reviews/friends/"+ user_id.to_s+"?movie_id=" + movie_id  )
			friends_review_request.on_complete do |friends_review_response|
				if friends_review_response.code >=200 && friends_review_response.code < 300
					friends_review_response_json = JSON.parse(friends_review_response.body)
					if friends_review_response_json['status'] == 1
						@friends_reviews = friends_review_response_json['data']['reviews']
						@friends_ratings = friends_review_response_json['data']['rating']
					end
				else
					@friends_reviews = nil
					@friends_ratings = nil
				end
			end
			hydra.queue(friends_review_request)
		end


		hydra.run

		
		data = {
			:friends_reviews => @friends_reviews ? @friends_reviews : [],
			:friends_ratings => @friends_ratings ? @friends_ratings : nil,
			:viewers_reviews => review_ratings[movie_id]['user']['reviews'],
			:critics_reviews => @critic_reviews,
			:me => review_ratings[movie_id]['user']['me']
		}
		
		apiresponse = {
			:status => status,
			:message => message,
			:data => data
		}
		render json: apiresponse.to_json
	end


	def set_fb_connections

		user_id = params[:user_id]
		fb_connection_ids = params[:fb_connection_ids]

		status  = 0
		message = 'Not Okay;'

		apiresponse = nil

		if user_id == nil
			message = 'User not logged in;'
		else
			status = 1
			message = 'Okay'
		end

		if fb_connection_ids == nil
			message = message + 'Facebook connections empty'
		else
			status = 1
			message = 'Okay'
		end

		if status == 0
			apiresponse = {
				:status => status,
				:message => message
			}

			render json: apiresponse.to_json
			return
		end

		p = params
		p = {
			:fb_connections => params[:fb_connection_ids]
		}

		the_request_url = USER_SERVICE + "connections/"+user_id.to_s+"/create_or_update"

		set_connection_response = Typhoeus::Request.post( the_request_url , :body => p )
		
		if set_connection_response.code >= 400
			logger.debug('Failed response for set_connection_response')
			status = 0
			message = 'Service failed with code '+set_connection_response.code.to_s
		elsif set_connection_response.code == 200
			set_connection_response_json = JSON.parse(set_connection_response.body)
			if set_connection_response_json['status'] == 1
				status = 1
				message = 'Okay'
			end
		end

		apiresponse = {
			:status => status,
			:message => message
		}
		render json: apiresponse.to_json
		
	end
end
