require "typhoeus"

class SearchController < ApplicationController

	def search
		query_string = params[:q]
		type = params[:type]
		apiresponse = nil
		status = 0
		message = 'Not okay'
		data = nil

		@q = query_string

		hydra = Typhoeus::Hydra.new
		search_request = Typhoeus::Request.new( MOVIE_SERVICE + "search" , method: :get, :body => { q: query_string } ) 
		search_request.on_complete do |search_response|
			if search_response.code >= 200 && search_response.code <= 300
				search_response_json = JSON.parse(search_response.body)
				if search_response_json['status'] == 1
					status = 1
					message = 'Okay'
					data = search_response_json['data']
					@movies = search_response_json['data']['movies']
					@celebrities = search_response_json['data']['celebrities']
				end
			else
				message = search_response_json.code.to_s + ' occurred'
			end
			#return
		end

		hydra.queue(search_request)
		hydra.run

		if type == "movies"
			apiresponse = @movies
		elsif type == "personalities"
			apiresponse = @celebrities
		end
		logger.debug(apiresponse)
		respond_to do |format|
	        	format.html { return }
	        	format.json { render json: apiresponse.to_json }
	    	end	
	end
end