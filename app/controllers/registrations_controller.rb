require 'digest/sha1'

class RegistrationsController < ApplicationController
	before_filter :no_login_required, :only => [:signup]
	def create
		apiresponse = nil
		status = 0
		data = []
		message = []

		if (	(params[:registration][:fname] == nil) ||
			(params[:registration][:lname] == nil) ||
			(params[:registration][:email] == nil) ||
			(params[:registration][:gender] == nil) ||
			(params[:registration][:password] == nil) ||
			(params[:registration][:password_confirmation] == nil))
			status =0
		else
			status = 1
		end


		if status == 1
			data = {
				'user' => params[:registration]
			}
			logger.debug(data)
			hydra = Typhoeus::Hydra.new
			register_user_response = Typhoeus::Request.post( USER_SERVICE + "users.json" , :body => data )

			if register_user_response.code >= 200 && register_user_response.code < 300
				register_user_response_json = JSON.parse(register_user_response.body)
				if register_user_response_json['status'] == 1
					status = 1
					data = register_user_response_json ['data']
					message = 'Okay'
				else
					status = 0
					message = register_user_response_json ['message']
					data = []
				end
			end			
		end
		
		apiresponse = {
			:status => status,
			:message => message,
			:data => data
		}
		logger.debug(apiresponse)
		render json: apiresponse.to_json
	end

	def create_via_fb
		#hydra = Typhoeus::Hydra.new
		status = 0
		message = 'Not okay'
		data = nil
		apiresponse = nil

		logger.debug(params)
		if (	(params[:user][:email] && params[:user][:email].length > 0) &&
			(params[:user][:fname] && params[:user][:fname].length > 0) &&
			(params[:user][:lname] && params[:user][:lname].length > 0) &&
			(params[:user][:fb_id] && params[:user][:fb_id].length > 0) &&
			(params[:user][:gender] && params[:user][:gender].length > 0))
			status = 1
			message = 'All params okay'
		end
		if status == 1
			register_user_request_response = Typhoeus::Request.post( USER_SERVICE + "createupdatefb" , :body => params )

			if register_user_request_response.code >= 200 && register_user_request_response.code  < 300
				register_user_request_response_json = JSON.parse(register_user_request_response.body)
				if register_user_request_response_json['status'] == 1

					current_user = register_user_request_response_json['data']	
					logger.debug(register_user_request_response_json)

					if params[:remember_me]
	      					cookies.permanent[:auth_token] = Digest::SHA1.hexdigest current_user['id'].to_s #current_user['auth_token']
	    				else
	      					cookies[:auth_token] = Digest::SHA1.hexdigest current_user['id'].to_s #current_user['auth_token']
	    				end

					self.after_sign_in(register_user_request_response_json['data'])

					status = 1
					message ='Create/Update via facebook succeded'
				else
					status = 0
					message = 'Create/Update via facebook failed'
				end	
			else
				status = 0
				message = 'Service Error'
			end			
		end

		apiresponse = {
			:status => status,
			:message => message,
			:data => data
		}
		render json: apiresponse.to_json
	end

	def signup
		
	end
end
