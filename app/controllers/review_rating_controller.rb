class ReviewRatingController  < ApplicationController
	def create
		rating = params[:rating]
		movie_id = params[:movie_id]

		status = 0
		message = 'Not okay'
		apiresponse = nil
		data = nil

		if rating != nil && movie_id != nil
			status = 1
			message = 'Okay'
		else
			message = 'Missing parameters'
		end

		parameters = nil

		parameters = {
			:rating => rating
		}		

		response = Typhoeus::Request.post( REVIEW_RATING_SERVICE + "/ratings/user/"+@current_user['id'].to_s+"/movie/" + movie_id.to_s , :body => parameters)

		if response.code >= 200 && response.code < 300
			status = 1
			message = 'Rating successful'
		end

		apiresponse = {
			:status => status,
			:mesage => message,
			:data => data
		}

		render json: apiresponse.to_json
	end

	def critic_review_rating
		movie_id = params[:movie_id]
		message = 'Not okay'
		data = nil
		apiresponse = nil
		status = 0
		
		hydra = Typhoeus::Hydra.new

		if movie_id != nil
			message = 'Okay'
			status = 1
		else
			message = 'Parameters are missing'
		end

		p = nil

		if status == 1
			if params[:next] != nil 
				p = {:next => params[:next]}
			end

			critic_review_ratings_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "critic_review_ratings/"+movie_id+".json", :body => p )
			critic_review_ratings_request.on_complete do |critic_review_ratings_response|
				logger.debug('critic_review_ratings_request')
				if critic_review_ratings_response.code >= 200 || critic_review_ratings_response.code < 300
					critic_review_ratings_response_json = JSON.parse(critic_review_ratings_response.body)

					if critic_review_ratings_response_json['status'] == 1
						data = critic_review_ratings_response_json['data']
					end
				end

			end
			hydra.queue(critic_review_ratings_request)

			hydra.run
		end

		apiresponse = {
			:status => status,
			:mesage => message,
			:data => data
		}

		render json: apiresponse.to_json	
	end
end