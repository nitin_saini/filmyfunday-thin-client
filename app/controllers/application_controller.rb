class ApplicationController < ActionController::Base
        protect_from_forgery  
        helper_method :admin?, :logged_in?, :fullname, :userLocationCity, :current_user, :facebook_id, :email
        before_filter :current_user, :except => :signout

        include ApplicationHelper

        TRUE = 'true'
        SUCCESS = 1
        USER_NOT_SIGNED_IN = 2

        protected

        def no_login_required
                if logged_in?
                        logger.debug(params)
                        redirect_to '/'
                end
        end

        def userLocationCity
                if request.location != nil && request.location.city != nil
                        return request.location.city
                else
                        return nil
                end
        end

        def require_login
                unless logged_in?
                        flash[:error] = "You must be logged in to access this section"
                        redirect_to '/signin' # halts request cycle
                end
        end

        def fullname
                if logged_in?
                        return @current_user['fname']+' '+@current_user['lname']
                else
                        return ''
                end
        end

        def facebook_id
                if logged_in?
                        return @current_user['fb_id']
                else
                        return ''
                end
        end

        def email
                if logged_in?
                        return @current_user['email']
                else
                        return ''
                end
        end

        def logged_in?
                bool = nil
                bool = @current_user ? true : false

                return bool
        end 

        def admin?
                if Rails.env.development?
                        return true
                elsif logged_in?
                        if (@current_user['fname'] == "Deborshi" && @current_user['lname'] == "Saha" && @current_user['fb_id'] == "735460577" )
                                return true
                        end 
                end
        end 

        def is_logged_in
                return @current_user ? true : false
        end


        def after_sign_in(c_user)
                Rails.cache.write(cookies[:auth_token], c_user)
        end

        def current_user

                if @current_user
                        return @current_user
                else
                        c_user = nil
                        if cookies[:auth_token]
                                c_user = Rails.cache.read(cookies[:auth_token])
                        end

                        if c_user != nil 
                                @current_user = c_user
                        else
                                @current_user = nil
                        end

                        return @current_user
                end
        end


        private
        def mobile_device?
                request.user_agent =~ /Mobile|webOs/
        end

        helper_method :mobile_device?

end
