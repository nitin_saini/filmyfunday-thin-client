class SessionsController < ApplicationController
	before_filter :no_login_required, :only => [:new]
	def new
		if @current_user
			redirect_to '/'
		end
	end

	def create
		status = 0
		message = []

		p = {
			'user' => params[:session]
		}
		user_authentication_response = Typhoeus::Request.post( USER_SERVICE + "finduser" , :body => p)

		if user_authentication_response.code == 200
			login_response_json = JSON.parse(user_authentication_response.body)
			if login_response_json['status']  != 1
				status = 0
				message << 'email/password did not match.'
			else
				current_user = login_response_json['data']

				if params[:remember_me]
      					cookies.permanent[:auth_token] = Digest::SHA1.hexdigest current_user['id'].to_s
    				else
      					cookies[:auth_token] = Digest::SHA1.hexdigest current_user['id'].to_s
    				end

    				# save the user in Redis
				self.after_sign_in(login_response_json['data'])

				status = 1
				message << 'Okay'				
			end
		end

		apiresponse = {
			:status => status,
			:message => message
		}

		respond_to do |format|
			format.html { redirect_to '/'}
			format.json { render json: apiresponse.to_json }
		end
	end

	def destroy
		
		@current_user = nil
		status = 1
		
		Rails.cache.write(cookies[:auth_token], nil)
		cookies.delete(:auth_token)

		apiresponse = {
			:status => status,
			:message => 'Sign out success'
		}

		respond_to do |format|
			format.html { redirect_to '/'}
			format.json { render json: apiresponse.to_json }
		end
	end

	private
	def user_session_create(params)
		
	end

end
