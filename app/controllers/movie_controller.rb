class MovieController < ApplicationController
    skip_before_filter :verify_authenticity_token

	def create
		require 'fileutils'
        require 'time'
    	tmp = params[:movie][:poster]
    	file = File.join("public", params[:movie][:poster].original_filename)
    	FileUtils.cp tmp.path, file

    	#setting the public id
    	downcased_name = params[:movie][:name].downcase
    	logger.debug(downcased_name)
    	#replacing spaces with dashes
    	public_id_string = downcased_name.gsub(/ /, '-')

    	logger.debug(public_id_string)
    	cloudinary_response = Cloudinary::Uploader.upload(file, :public_id => public_id_string)
    	logger.debug('cloudinary response' + cloudinary_response.to_s)
    	FileUtils.rm file
    	params[:movie][:poster_url] = cloudinary_response['public_id']
    	
        playtime_in_minutes = params[:movie][:playtime][:hours].to_i * 60 + params[:movie][:playtime][:minutes].to_i
        
        # delete the poster parameter which contained the image
        params[:movie].delete :poster

        # delete playtime which was broken up into hours and minutes
        params[:movie].delete :playtime

        # add playtime in minutes
        params[:movie][:playtime] = playtime_in_minutes

        # change the format and make it as per DB
        str3 = DateTime.strptime( params[:movie][:release_date], '%m/%d/%Y').to_time

        # modify movie release date as per DB backend
        params[:movie][:release_date] = str3
    	
        
        # make a post request to backend
        request = Typhoeus::Request.post(MOVIE_SERVICE + "movies.json", :params => params )
        
        if request.code > 300
            logger.debug('Error '+ request.code.to_s)
            flash[:notice] = 'Error ' + request.code.to_s
        elsif request.code == 200 || request.code == 201
            logger.debug('Error '+ request.code.to_s)
            @details_of_added_movie = JSON.parse(request.body)

            # flash notice
            str = "#{render_to_string :partial => '/layouts/detailed_output_of_added_movie'}"
            flash[:notice] = str     
        end

        # redirect it to profile
        redirect_to '/profile'

	end
end