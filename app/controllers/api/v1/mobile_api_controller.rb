require "typhoeus"

module Api
        module V1
                class MobileApiController < ApplicationController

                        respond_to :json

                        def home

                                hydra = Typhoeus::Hydra.new

                                movie_id_list = nil

                                list_of_movies_request = Typhoeus::Request.new( MOVIE_SERVICE + "movies.json?segregated=1")
                                list_of_movies_request.on_complete do |list_of_movies_response|
                                        list_of_movies_response_json = JSON.parse(list_of_movies_response.body)
                                        if list_of_movies_response_json['status'] == 1
                                                @upcoming = list_of_movies_response_json['data']['upcoming']
                                                @released = list_of_movies_response_json['data']['released']
                                                movie_id_array = Array.new
                                                movie_array = Array.new

                                                @released.each do |nmr|
                                                        movie_id_array << nmr['id']
                                                        movie_array << nmr
                                                        
                                                end

                                                arr_string = movie_id_array.join(",")

                                                movie_array_hash = movie_array.index_by{ |element| element['id'].to_s }

                                                ratings_for_movies_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "ratings/movie?movie_ids="+ arr_string)
                                                ratings_for_movies_request.on_complete do |ratings_for_movies_response|
                                                        ratings_for_movies_response_json = JSON.parse(ratings_for_movies_response.body)
                                                        if ratings_for_movies_response_json['status'] == 1
                                                                movie_id_array.each do |m_id|
                                                                        movie = movie_array_hash[m_id.to_s]
                                                                        movie.delete 'created_at'
                                                                        movie.delete 'updated_at'
                                                                        movie['viewer_rating'] = ratings_for_movies_response_json['data'][m_id.to_s]['user']['rating'].to_f.round(2) 
                                                                        movie['critic_rating'] = ratings_for_movies_response_json['data'][m_id.to_s]['critic']['rating'].to_f.round(2) 
                                                                        if movie['viewer_rating'] == 0
                                                                                movie['viewer_rating'] = 0
                                                                        end
                                                                        if movie['critic_rating'] == 0
                                                                                movie['critic_rating'] = 0
                                                                        end
                                                                        logger.debug(movie)
                                                                end
                                                        end
                                                end             
                                                hydra.queue(ratings_for_movies_request)
                                                hydra.run               
                                        end     
                                end
                                hydra.queue(list_of_movies_request)
                                hydra.run

                                data = {
                                        :released => @released,
                                        :upcoming => @upcoming
                                }

                                apiresponse = {
                                        :status => 1,
                                        :data => data
                                }

                                respond_with apiresponse
                        end

                        def movie_reviews
                                movie_id = params[:movie_id]
                                user_id = params[:user_id]

                                status = 0
                                message = 'Not okay'
                                data = nil

                                review_ratings = nil

                                apiresponse = nil
                                
                                hydra = Typhoeus::Hydra.new

                                # critic review request
                                if user_id != nil
                                        review_rating_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "ratings/movie?movie_ids="+movie_id+"&viewer_id="+user_id.to_s)
                                else
                                        review_rating_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "ratings/movie?movie_ids="+movie_id)
                                end
                                
                                
                                review_rating_request.on_complete do |review_rating_response|
                                        if review_rating_response.code >= 200  && review_rating_response.code < 300
                                                review_rating_response_json = JSON.parse(review_rating_response.body)
                                                status = 1
                                                message = 'Okay'
                                                review_ratings = review_rating_response_json['data']
                                        else
                                                message = 'Service error '+review_rating_response.code.to_s
                                                review_ratings = nil
                                        end                     
                                end
                                hydra.queue(review_rating_request)

                                critic_review_ratings_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "critic_review_ratings.json", :body => {:movie_id => movie_id} )
                                critic_review_ratings_request.on_complete do |critic_review_ratings_response|
                                        if critic_review_ratings_response.code >= 200 || critic_review_ratings_response.code < 300
                                                critic_review_ratings_response_json = JSON.parse(critic_review_ratings_response.body)

                                                if critic_review_ratings_response_json['status'] == 1
                                                        @critic_reviews = critic_review_ratings_response_json['data']
                                                else
                                                        @critic_reviews = nil
                                                end
                                        end

                                end
                                hydra.queue(critic_review_ratings_request)

                                if user_id != nil
                                        friends_review_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE  + "reviews/friends/"+ user_id.to_s+"?movie_id=" + movie_id  )
                                        friends_review_request.on_complete do |friends_review_response|
                                                if friends_review_response.code >=200 && friends_review_response.code < 300
                                                        friends_review_response_json = JSON.parse(friends_review_response.body)
                                                        if friends_review_response_json['status'] == 1
                                                                @friends_reviews = friends_review_response_json['data']['reviews']
                                                                @friends_ratings = friends_review_response_json['data']['rating']
                                                        end
                                                else
                                                        @friends_reviews = nil
                                                        @friends_ratings = nil
                                                end
                                        end
                                        hydra.queue(friends_review_request)
                                end


                                hydra.run

                                
                                data = {
                                        :friends_reviews => @friends_reviews ? @friends_reviews : [],
                                        :friends_ratings => @friends_ratings ? @friends_ratings : nil,
                                        :viewers_reviews => review_ratings[movie_id]['user']['reviews'],
                                        :critics_reviews => @critic_reviews,
                                        :me => review_ratings[movie_id]['user']['me']
                                }
                                
                                apiresponse = {
                                        :status => status,
                                        :message => message,
                                        :data => data
                                }
                                respond_with apiresponse
                        end

                        def login
                                status = 0
                                message = []
                                current_user = nil

                                p = {
                                        'user' => params[:session]
                                }
                                user_authentication_response = Typhoeus::Request.post( USER_SERVICE + "finduser" , :body => p)

                                if user_authentication_response.code == 200
                                        login_response_json = JSON.parse(user_authentication_response.body)
                                        if login_response_json['status']  != 1
                                                status = 0
                                                message << 'email/password did not match.'
                                        else
                                                current_user = login_response_json['data']
                                                status = 1
                                                message << 'Okay'                               
                                        end
                                end

                                apiresponse = {
                                        :status => status,
                                        :message => message,
                                        :data => current_user
                                }

                                render json: apiresponse.to_json                             
                        end
                end
        end
end