require "typhoeus"

class PeopleController < ApplicationController
	def people_details
		people_id = params[:people_id]
		apiresponse = nil
		status = 0
		message = 'Not okay'
		data = nil

		movies = []

		hydra = Typhoeus::Hydra.new
		persons_work_request = Typhoeus::Request.new( MOVIE_SERVICE + "people/"+people_id.to_s+".json" , method: :get ) 
		persons_work_request.on_complete do |persons_work_response|
			if persons_work_response.code >= 200 && persons_work_response.code <= 300
				persons_work_response_json = JSON.parse(persons_work_response.body)
				if persons_work_response_json['status'] == 1
					status = 1
					message = 'Okay'
					data = persons_work_response_json['data']
					logger.debug(persons_work_response_json['data'])
					@person_name = persons_work_response_json['data']['name']
					@movies_acted = persons_work_response_json['data']['movies_acted']
					@movies_directed = persons_work_response_json['data']['movies_directed']
				end
			else
				message = persons_work_response.code.to_s + ' occurred'
			end
			return
		end


		hydra.queue(persons_work_request)
		hydra.run
	end
end