require "typhoeus"

class IndexController < ApplicationController
	include IndexHelper
	before_filter :require_login, :only => :account
	before_filter :no_login_required, :only => :forgot_password 

	helper_method :get_badge_class_name_based_on_rating
	helper_method :get_bar_class_name_based_on_rating

	def remove_from_watchlist
		movie_id = params[:movie_id]
		status = 0
		message = 'Not Okay'
		apiresponse = nil

		if movie_id == nil
			message = 'Missing parameters'
			apiresponse = {
				:status => status,
				:message => reason
			}

			render json: apiresponse.to_json
			return			
		end

		hydra = Typhoeus::Hydra.new
		remove_from_watchlist_request = Typhoeus::Request.new( USER_SERVICE + "users/"+@current_user['id'].to_s+"/watchlist/"+movie_id+"/remove", method: :post, :body => nil) 
		remove_from_watchlist_request.on_complete do |remove_from_watchlist_response|

			if remove_from_watchlist_response.code >= 200 && remove_from_watchlist_response.code <= 300

				remove_from_watchlist_json = JSON.parse(remove_from_watchlist_response.body)
				logger.debug(remove_from_watchlist_json)
				if remove_from_watchlist_json['status'] == 1
					status = 1
					message = remove_from_watchlist_json['reason']
				else
					message = remove_from_watchlist_json['reason']
				end
			else
				message = remove_from_watchlist_response.code.to_s + ' occurred'
			end

			apiresponse = {
				:status => status,
				:message => message
			}

			render json: apiresponse.to_json
			return
		end

		hydra.queue(remove_from_watchlist_request)
		hydra.run		

	end


	#required movie_id
	def watched_a_movie_from_watchlist
		movie_id = params[:movie_id]
		status = 0
		reason = nil
		apiresponse = nil

		if movie_id == nil
			reason = 'Missing parameters'
			apiresponse = {
				:status => status,
				:reason => reason
			}

			render json: apiresponse.to_json
			return			
		end

		hydra = Typhoeus::Hydra.new
		watched_a_movie_from_watchlist_request = Typhoeus::Request.new( USER_SERVICE + "users/"+@current_user['id'].to_s+"/watchlist/"+movie_id+"/watched", method: :post, :body => nil) 
		watched_a_movie_from_watchlist_request.on_complete do |watched_a_movie_from_watchlist_response|
			if watched_a_movie_from_watchlist_response.code >= 200 && watched_a_movie_from_watchlist_response.code <= 300
				watched_a_movie_from_watchlist_json = JSON.parse(watched_a_movie_from_watchlist_response.body)
				if watched_a_movie_from_watchlist_json['status'] == 1
					status = 1
					reason = watched_a_movie_from_watchlist_json['reason']
					data = movie_id
				end
			else
				reason = watched_a_movie_from_watchlist_response.code.to_s + ' occurred'
			end

			apiresponse = {
				:status => status,
				:message => reason,
				:data => movie_id
			}

			render json: apiresponse.to_json
			return
		end

		hydra.queue(watched_a_movie_from_watchlist_request)
		hydra.run		

	end

	def add_critic_review_rating
		logger.debug(params)
		status = 0
		reason = nil
		apiresponse = nil

		if params[:critic_review_rating] == nil		
			apiresponse = {
				:status => status,
				:reason => reason
			}

			render json: apiresponse.to_json
			return
		end

		hydra = Typhoeus::Hydra.new
		add_critic_review_rating_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "critic_review_ratings.json", method: :post, :body => params) 
		add_critic_review_rating_request.on_complete do |add_critic_review_rating_response|
			if add_critic_review_rating_response.code >= 200 && add_critic_review_rating_response.code <= 300
				add_critic_review_rating_json = JSON.parse(add_critic_review_rating_response.body)
				if add_critic_review_rating_json['status'] == 1
					status = 1
					reason = add_critic_review_rating_json['reason']
				end
			else
				reason = add_critic_review_rating_response.code.to_s + ' occurred'
			end

			apiresponse = {
				:status => status,
				:reason => reason,
			}

			render json: apiresponse.to_json
			return
		end

		hydra.queue(add_critic_review_rating_request)
		hydra.run	
	end

	def add_critic
		logger.debug(params)
		status = 0
		reason = nil
		apiresponse = nil

		if params[:critic] == nil		
			apiresponse = {
				:status => status,
				:reason => reason
			}

			render json: apiresponse.to_json
			return
		end

		hydra = Typhoeus::Hydra.new
		add_critic_request = Typhoeus::Request.new( USER_SERVICE + "critics.json", method: :post, :body => params) 
		add_critic_request.on_complete do |add_critic_response|
			if add_critic_response.code >= 200 && add_critic_response.code <= 300
				add_critic_response_json = JSON.parse(add_critic_response.body)
				if add_critic_response_json['status'] == 1
					status = 1
					reason = 'Critic name was successfully added'
					data = add_critic_response_json['data']
				end
			else
				reason = add_critic_response.code.to_s + ' occurred'
				data = []
			end

			apiresponse = {
				:status => status,
				:reason => reason,
				:data => data
			}

			render json: apiresponse.to_json
			return
		end

		hydra.queue(add_critic_request)
		hydra.run			
	end

	def theaters_near_location

		logger.debug(request.location)
		location = params[:near]
		location = location.gsub!(' ','+')
		hydra = Typhoeus::Hydra.new
		apiresponse = {
			:status => 0
		}
		theaters_around_me_request = Typhoeus::Request.new(  "http://shielded-falls-5778.herokuapp.com/theaters.json?city="+params[:near])
		theaters_around_me_request.on_complete do |theaters_around_me_response|
			if theaters_around_me_response.code >= 200 && theaters_around_me_response.code <= 300
				@theaters_around_me_json = JSON.parse(theaters_around_me_response.body)
			end
			
			# render json: @theaters_around_me_json.to_json
			# return
		end

		hydra.queue(theaters_around_me_request)
		hydra.run
		# render json: apiresponse.to_json
	end

	def load_more_movies
		status = 0
		reason = 'Could not load'
		data = nil
		apiresponse = nil

		if session[:next_page] != nil
			#hydra = Typhoeus::Hydra.new
			#movie_load_request = Typhoeus::Request.new( session[:next_page])
			movie_load_response = Typhoeus::Request.get( session[:next_page])
			logger.debug(session[:next_page])
			movie_releases = JSON.parse(movie_load_response.body)

			if movie_releases['status'].to_i == 1
				status = 1
				data = movie_releases['data'] 
				session[:next_page] =  movie_releases['next_page']
				apiresponse = {
					:status => status,
					:data => data
				}
			else
				status = 0
				reason = 'Could not load'
				apiresponse = {
					:status => status,
					:reason => reason
				}
			end	

			render json: apiresponse.to_json
			return
		end
		apiresponse = {
			:status => status,
			:reason => reason
		}
		render json: apiresponse.to_json
	end

	def do_save_account_email_preferences
		logger.debug(params)
		status = 0
		message = "Couldn't save email preference"

		params[:email_preference].delete :action
		params[:email_preference].delete :controller

		email_preference_response = Typhoeus::Request.put( USER_SERVICE + "email_preferences/"+@current_user['id'].to_s+".json" , :params => params)
		if email_preference_response.code >= 200 && email_preference_response.code < 300
			email_preference_response_json = JSON.parse(email_preference_response.body)
			if email_preference_response_json['status'] == 1
				status = 1
				message = "Okay"
			end
		end
		
		apiresponse = {
			:status => status,
			:message => message
		}

		render json: apiresponse.to_json
	end

	def settings
		hydra = Typhoeus::Hydra.new

		email_preferences_request = nil
		watchlist_request = nil

		if logged_in?
			my_watchlist_request = Typhoeus::Request.new(USER_SERVICE + "user/"+@current_user['id'].to_s+"/watchlist")
			my_watchlist_request.on_complete do |my_watchlist_response|
				if my_watchlist_response.code == 200
					my_watchlist_response_json = JSON.parse(my_watchlist_response.body)
					@my_watchlist = my_watchlist_response_json['data']
					logger.debug(@my_watchlist)
				else
					@my_watchlist = nil
				end
			end
			hydra.queue(my_watchlist_request)

			email_preferences_request = Typhoeus::Request.new( USER_SERVICE + "email_preferences/"+@current_user['id'].to_s+".json")
			email_preferences_request.on_complete do |email_preferences_response|
				if email_preferences_response.code >= 200  && email_preferences_response.code < 300
					email_preferences_json = JSON.parse(email_preferences_response.body)
					if email_preferences_json['status'] == 1
						# process rest of the data
						@email_preferences = email_preferences_json['data'] 
					else
						# ignore it
						@email_preferences = nil
					end
				else
					@email_preferences = nil
				end	
			end
			hydra.queue(email_preferences_request)

			get_all_movies_request = Typhoeus::Request.new( MOVIE_SERVICE + "movies_last_week")
			get_all_movies_request.on_complete do |get_all_movies_response|

				if get_all_movies_response.code >= 200 && get_all_movies_response.code < 300 
					get_all_movies_response_json = JSON.parse(get_all_movies_response.body)
					logger.debug(get_all_movies_response_json)
					if get_all_movies_response_json['status'].to_i == 1
					 	@movies_last_week = get_all_movies_response_json['data']
					else
					 	logger.debug('>>>>>>>>>>>else>>>>>>>>>>>>>')
					end 
				else
					@movies_last_week = nil
				end			
			end
			hydra.queue(get_all_movies_request)

			get_all_critics_request = Typhoeus::Request.new( USER_SERVICE + "critics.json")
			get_all_critics_request.on_complete do |get_all_critics_response|
				if get_all_critics_response.code >= 200 && get_all_critics_response.code < 300 
					get_all_critics_json = JSON.parse(get_all_critics_response.body)
					if get_all_critics_json['status'].to_i == 1
					 	@critics = get_all_critics_json['data']
					end 
				else
					@critics = nil
				end			
			end

			hydra.queue(get_all_critics_request)

			my_reviews_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "viewer_review/"+@current_user['id'].to_s+".json")
			my_reviews_request.on_complete do |my_reviews_response|
				if my_reviews_response.code == 200 
					if my_reviews_response.body
						my_reviews_response_json = JSON.parse(my_reviews_response.body)
						if my_reviews_response_json['status'].to_i == 1
							@my_reviews = my_reviews_response_json['data']

							if @my_reviews && @my_reviews.length != 0

								arr = []
								
								@my_reviews.each do |mr|
									arr << mr['movie_id']
								end

								#storing it as comma seperated
								movie_comma_seperated = arr.join(',')

								# Movie look up BEGINS
								params = {
									:ids => movie_comma_seperated,
									:genre => 1,
									:synopsis => 1
								}
								movie_with_ids_request = Typhoeus::Request.new( MOVIE_SERVICE + "movies.json", :method => 'GET', :body => params )

								movie_with_ids_request.on_complete do |movie_with_ids_response|
									if movie_with_ids_response.code == 200 

										movie_with_ids_response_json = JSON.parse(movie_with_ids_response.body)

										movies_with_id_array = nil

										if movie_with_ids_response_json['status'].to_i == 1
											movies_with_id_array = movie_with_ids_response_json['data']
										end
										@movies_hash = movies_with_id_array.index_by { |element|  element['id']}

										logger.debug(@movies_hash)
									else
										@movies_hash = nil
									end	
								end

								hydra.queue(movie_with_ids_request)
								hydra.run
								# Movie look up ENDS
							end
						else
							@my_reviews = nil
						end
					else
						@my_reviews = nil
					end

				else
					@my_reviews = nil
				end			
			end
			hydra.queue(my_reviews_request)

			hydra.run
		end
	end
	
	def forgot_password
		reset_hash = params[:rh]
		if reset_hash != nil && reset_hash.length == 40
			# check backend
			check_hash_validity_response = Typhoeus::Request.get( USER_SERVICE + "user/check_reset_hash/"+reset_hash.to_s )
			check_hash_validity_response_json = JSON.parse(check_hash_validity_response.body)
			logger.debug(check_hash_validity_response_json)
			logger.debug(check_hash_validity_response.code)
			if check_hash_validity_response_json['status'] == 1
				# render change password
				@hash = reset_hash
				render 'reset_password'
				return
			elsif check_hash_validity_response_json['status'] == 0
				flash[:error] = check_hash_validity_response_json['reason']
			end
		end
	end

	def reset_password

		status = 0
		message = 'Not okay'
		hydra = Typhoeus::Hydra.new


		reset_password_request = Typhoeus::Request.new( USER_SERVICE + "user/reset_password" , method: :post, :body => params )
		reset_password_request.on_complete do |reset_password_response|
			if reset_password_response.code >= 200  && reset_password_response.code < 300
				reset_password_response = JSON.parse(reset_password_response.body)
				if reset_password_response['status'] == 1
					# process rest of the data
					status = 1
					message = reset_password_response['message']
				else
					status = 0
					message = reset_password_response['message'] 
				end
			else
				status = 0
				message = 'Error : '+reset_password_response.code.to_s
			end	
		end
		hydra.queue(reset_password_request)
		hydra.run

		apiresponse = {
			:status => status,
			:message => message
		}

		render json: apiresponse.to_json
	end

	def send_password_reset_email
		logger.debug(params[:forgotPasswordEmail])
		status = 0
		reason = 'Resetting your password did not succeed!'
		if is_a_valid_email?(params[:forgotPasswordEmail])
			# send the request to backend user service
			forgot_password_response = Typhoeus::Request.post( USER_SERVICE + "user/forgot_password" , :body => params)
			if forgot_password_response.code == 200
				forgot_password_response_json = JSON.parse(forgot_password_response.body)
				if forgot_password_response_json['status'] == 1
					status = 1
					reason = forgot_password_response_json['reason']
				else
					status = 0
					reason = forgot_password_response_json['reason']
				end
			end
		else
			reason = 'Please enter a valid email address'
		end	

		apiresponse = {
			:status => status,
			:reason => reason
		}
		render json: apiresponse.to_json
	end

	def home

		movie_releases = nil

		if movie_releases == nil
			logger.debug('New releases not in cache. Fetching...')
			movies_response = Typhoeus::Request.get( MOVIE_SERVICE + "movies.json?segregated=1")
			if movies_response.code >= 200 && movies_response.code < 300
				movie_releases = JSON.parse(movies_response.body)
				if movie_releases['status'] == 1
					@upcoming_releases = movie_releases['data']['upcoming']
					@new_releases = movie_releases['data']['released']
					session[:next_page] = movie_releases['data']['next_page']
				end
			else
				@new_releases = nil
				@upcoming_releases = nil
			end
		end

		if @new_releases == nil || @upcoming_releases == nil
			#exception
			not_found
			return
		end

		@arr = Array.new
		@new_releases.each do |nmr|
			@arr << nmr['id']
		end

		arr_string = @arr.join(",")	

		hydra = Typhoeus::Hydra.new

		new_release_movie_rating_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "ratings/movie?movie_ids="+ arr_string)
		
		new_release_movie_rating_request.on_complete do |new_release_movie_rating_response|
			if new_release_movie_rating_response.code >= 200 && new_release_movie_rating_response.code < 300
				new_release_movie_rating_response_json = JSON.parse(new_release_movie_rating_response.body)
				if new_release_movie_rating_response_json['status'] == 1
				 	@released_movie_rating = new_release_movie_rating_response_json['data']
				else
					@released_movie_rating = 'Failed'
				end 
			end
		end
		hydra.queue(new_release_movie_rating_request)

                url  = "movies/14/ago"
                last_week_release_request = Typhoeus::Request.new( MOVIE_SERVICE + url )
                last_week_release_request.on_complete do |last_week_release_response|
                        last_week_release_response_json = JSON.parse(last_week_release_response.body)
                        if last_week_release_response_json['status'] == 1
                                @opening_movies = last_week_release_response_json['data']
                        end
                end

                hydra.queue(last_week_release_request)
		
		@now_playing = []
		params = {
			:playing => 1
		}
		now_playing_request = Typhoeus::Request.new( SHOWTIME_SERVICE + "showtimes.json", :body => params)
		now_playing_request.on_complete do |now_playing_response|

			if now_playing_response.code >= 200 || now_playing_response.code < 300
				now_playing_response_json = JSON.parse(now_playing_response.body)

				if now_playing_response_json['status'] == 1
					@now_playing = now_playing_response_json['data']
				end
			end

		end
		hydra.queue(now_playing_request)

		@ratings_of_top_movies = []
		@top_rated_movies = []
		params = {
			:tr => 1
		}
		top_ratings_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "/ratings", :body => params)
		top_ratings_request.on_complete do |top_ratings_response|

			if top_ratings_response.code >= 200 || top_ratings_response.code < 300
				top_ratings_response_json = JSON.parse(top_ratings_response.body)

				if top_ratings_response_json['status'] == 1
					@ratings_of_top_movies = top_ratings_response_json['data']
					array_of_movie_ids = []

					@ratings_of_top_movies.each do |element|
						array_of_movie_ids << element['movie_id']
					end

					params = {
						:ids => array_of_movie_ids.join(',')
					}

					movie_with_ids_request = Typhoeus::Request.new( MOVIE_SERVICE + "movies.json", :method => 'GET', :body => params )

					movie_with_ids_request.on_complete do |movie_with_ids_response|
						if movie_with_ids_response.code == 200 

							movie_with_ids_response_json = JSON.parse(movie_with_ids_response.body)

							movies_with_id_array = nil

							if movie_with_ids_response_json['status'] == 1
								temp = movie_with_ids_response_json['data']
								@top_rated_movies = temp.index_by { |e| e['id'] }
							end
						end	
					end
					hydra.queue(movie_with_ids_request)
					hydra.run
				end
			end

		end
		hydra.queue(top_ratings_request)

		hydra.run

	end

	def movie_review
		
		movie_id = params[:movie_id]
		
		@friends_reviews = nil
 
		hydra = Typhoeus::Hydra.new

		url = nil
		@latitude = nil
		@longitude = nil

		if request.location != nil && request.location.latitude && request.location.longitude
			@latitude = request.location.latitude.to_f
			@longitude = request.location.longitude.to_f
		end

		@zones = []
		@showtimes = []
		zones_request =Typhoeus::Request.new(SHOWTIME_SERVICE+"zones", :method =>'get')
		zones_request.on_complete do |zones_response|
			if zones_response.code >=200 && zones_response.code < 300
				zones_response_json = JSON.parse(zones_response.body)
				if zones_response_json['status'] == 1
					@zones = zones_response_json['data']
					if @zones
						@in_zone = get_zone_id( @zones, @latitude, @longitude)
					end
					
					if @in_zone != nil
						@showtimes = get_showtimes(@in_zone['id'], movie_id)
					end
					
				end
			else
				@zones = []	
			end
		end
		hydra.queue(zones_request)

		# critic review request
		if self.is_logged_in
			url = REVIEW_RATING_SERVICE + "ratings/movie?movie_ids="+movie_id+"&viewer_id="+@current_user['id'].to_s
		else
			url = REVIEW_RATING_SERVICE + "ratings/movie?movie_ids="+movie_id
		end
		
		@ratings = nil
		ratings_request = Typhoeus::Request.new(url)
		
		ratings_request.on_complete do |response|
			if response.code == 200 
				ratings_response_json = JSON.parse(response.body)
				@ratings = ratings_response_json['data']
			else
				@ratings = nil
			end			
		end
		hydra.queue(ratings_request)

		url = MOVIE_SERVICE + "movies/"+movie_id+".json?starcast=1"
		
		get_movie_details_request = Typhoeus::Request.new( url )

		get_movie_details_request.on_complete do |response|
			logger.debug('get_movie_details_request')
			if response.code >= 200 && response.code < 300
				begin
					movie_details_json = JSON.parse(response.body)
					if movie_details_json['status'] == 1
						# all okay
						@movie_details = movie_details_json['data']
					else
						#raise exception
						not_found
					end
				rescue JSON::ParserError 
					not_found
				end
			end
		end
		hydra.queue(get_movie_details_request)

		# request
		if @current_user

			p = {
				:movie_id => movie_id,
				:user_id => @current_user['id']
			}
			friends_watchlist_request = Typhoeus::Request.new( USER_SERVICE + 'watchlist/of_a_movie', :method =>'post', :body => p  )
			friends_watchlist_request.on_complete do |friends_watchlist_response|
				
				if friends_watchlist_response.code >= 200 || friends_watchlist_response.code < 300
					friends_watchlist= JSON.parse(friends_watchlist_response.body)
					@want_to_watch = friends_watchlist['data']['want_to_watch']
					@watched = friends_watchlist['data']['watched']
					@in_watchlist = friends_watchlist['data']['in_watchlist']
					@watchlistId = friends_watchlist['data']['watchlist_id']
					@have_watched = friends_watchlist['data']['have_watched']
					@count = friends_watchlist['data']['count']
				else
					@want_to_watch = nil
					@watched = nil
					@in_watchlist = nil
					@count = nil
				end
			end

			hydra.queue(friends_watchlist_request)
		end

		# request
		@friends_reviews = []
		if @current_user && @current_user['fb_id']
			
			friends_review_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE  + "reviews/friends/"+ @current_user['id'].to_s+"?movie_id=" + movie_id  )
			friends_review_request.on_complete do |friends_review_response|
				logger.debug('friends_review_request')
				if friends_review_response.code >=200 && friends_review_response.code < 300
					friends_review_response_json = JSON.parse(friends_review_response.body)
					if friends_review_response_json['status'] == 1
						@friends_reviews = friends_review_response_json['data']['reviews']
					end
				end
			end
			hydra.queue(friends_review_request)
		end

		p = {
			:movie_id => movie_id
		}
		@viewer_reviews = []
		get_viewer_reviews_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "viewers/review?movie_id=" + movie_id  )
		get_viewer_reviews_request.on_complete do |get_viewer_reviews_response|
			logger.debug('get_viewer_reviews_request')
			if get_viewer_reviews_response.code == 200 
				viewer_reviews = JSON.parse(get_viewer_reviews_response.body)
				if viewer_reviews['status'] == 1					
					if viewer_reviews['data'] && viewer_reviews['data'].length > 0
						@viewer_reviews = viewer_reviews['data']
					else
						@viewer_reviews = []
					end
				else
					@viewer_reviews = []
				end
			else
				logger.error(get_viewer_reviews_response.code)
				@viewer_reviews = []
			end
		end
		hydra.queue(get_viewer_reviews_request)
		
		@critic_reviews = []
		critic_review_ratings_request = Typhoeus::Request.new( REVIEW_RATING_SERVICE + "critic_review_ratings/"+movie_id+".json", :body => p )
		critic_review_ratings_request.on_complete do |critic_review_ratings_response|
			logger.debug('critic_review_ratings_request')
			if critic_review_ratings_response.code >= 200 || critic_review_ratings_response.code < 300
				critic_review_ratings_response_json = JSON.parse(critic_review_ratings_response.body)

				if critic_review_ratings_response_json['status'] == 1
					@critic_reviews = critic_review_ratings_response_json['data']['reviews']
					@critic_review_next = critic_review_ratings_response_json['data']['next']
				else
					@critic_reviews = []
				end
			end

		end
		hydra.queue(critic_review_ratings_request)

		hydra.run
	end

	def about
		
	end
	
	def test_email
		#testing Email
		@user = {
			:email => 'deborshi.saha@gmail.com',
			:fname => 'Deborshi',
			:lname => 'Saha'
		}

		EmailNotifier.signup_email(@user).deliver
		data = {
			:dummy => 'hi'
		}
		render json: data.to_json
	end

	private
	def is_a_valid_email?(email)
    		email_regex = %r{^[a-zA-Z0-9]+\.*[a-zA-Z0-9]*@[a-zA-Z0-9]+\.[a-zA-Z]+$}xi
    		(email =~ email_regex)
	end

	def not_found
		begin
    			raise ActionController::RoutingError, 'Page Not Found'
    		rescue ActionController::RoutingError
    			redirect_to '/404.html'
    		end
  	end
end
