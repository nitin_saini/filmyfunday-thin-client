class ShowtimesController < ApplicationController

	include IndexHelper
	# GET /showtimes 
	def get_showtimes_for_movie_in_zone
		status = 0
		message = 'Not okay'
		data = nil
		apiresponse = nil
		zone_id = params[:zone_id]
		movie_id = params[:movie_id]
		if zone_id && movie_id
			message = 'Okay'
			status = 1
		end

		if status == 1
			data = get_showtimes( zone_id, movie_id)
		end
		apiresponse = {
			:status => status,
			:message => message,
			:data => data
		}

		render json: apiresponse.to_json
	end
end