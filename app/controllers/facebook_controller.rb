class FacebookController < ApplicationController
	def set_fb_connections
		if params[:fb_connection_ids] == nil
			apiresponse = {
				:status => 1
			}
			render json: apiresponse.to_json
			return
		end
		p = params
		p = {
			:fb_connections => params[:fb_connection_ids]
		}

		the_request_url = USER_SERVICE + "connections/"+@current_user['fb_id'].to_s+"/create_or_update"
		logger.debug(the_request_url)
		logger.debug(params[:fb_connection_ids])
		set_connection_response = Typhoeus::Request.post( the_request_url , :body => p )
		apiresponse = nil
		
		if set_connection_response.code >= 400
			logger.debug('Failed response for set_connection_response')
			apiresponse = {
				:status => 0
			}
		elsif set_connection_response.code == 200
			set_connection_response_json = JSON.parse(set_connection_response.body)
			if set_connection_response_json['status'] == 1
				apiresponse = {
					:status => 1,
					:message => set_connection_response_json['message']
				}
			end
		end
		render json: apiresponse.to_json
	end
end