class PostController < ApplicationController
	def new
		status = 0
		message = 'Not okay'
		data = nil
		apiresponse = nil
		p = nil

		isLoggedIn = logged_in?

		if isLoggedIn
			status = 1
			p = params
			message = 'Okay'
			p[:viewer_review][:viewer_id] = @current_user['id']
			p[:viewer_review][:viewer_name] = @current_user['fname'] + ' ' + @current_user['lname'] 
		else
			status = 0
			message = 'User not logged in'
		end

		if status == 1
			review_post_response = Typhoeus::Request.post( REVIEW_RATING_SERVICE + "reviews/viewer/create" , :body => p)
			
			if review_post_response.code >= 200 && review_post_response.code < 300
				review_post_response_json = JSON.parse(review_post_response.body)
				if review_post_response_json['status'] == 1
					posted_review = review_post_response_json['data']
					status = 1
					data = posted_review
				else
					status = 0
					message = 'User service returned status 0'
				end
			else
				status = 0
				message = review_post_response.code.to_s + ' occurred'
			end
		end
		apiresponse = {
			:status => status,
			:message => message,
			:data => data
		}
		render json: apiresponse.to_json
	end

	# for single user
	def get_awaitingness
		movie_id = params[:m_id]
		user_id = @current_user['id']
		
		p = {
			:m_id => movie_id,
			:u_id => user_id
		}

		get_awaiting_response = Typhoeus::Request.get( MOVIE_SERVICE + "awaiting" , :params => p )
		get_awaiting_response_json = JSON.parse(get_awaiting_response.body)
		logger.debug (get_awaiting_response_json)
		render :json => get_awaiting_response_json.to_json
	end

	# set for single user
	def set_awaitingness
		movie_id = params[:movie_id]
		status = 0
		message = 'User not logged in'
		apiresponse = nil

		if self.is_logged_in
			message = 'User logged in'

			p = params
			p = {
				:m_id => movie_id,
				:u_id => @current_user['id']
			}
			# post  awaiting
			#add_want_to_watchers_list_of_movie_request = Typhoeus::Request.post( MOVIE_SERVICE + "awaiting" , :body => p)

			hydra = Typhoeus::Hydra.new
			add_to_my_watchlist_request = Typhoeus::Request.new( USER_SERVICE + "users/"+@current_user['id'].to_s+"/watchlist/"+movie_id.to_s , method: :post, :body => nil) 
			add_to_my_watchlist_request.on_complete do |add_to_my_watchlist_response|
				if add_to_my_watchlist_response.code >= 200 && add_to_my_watchlist_response.code <= 300
					add_to_my_watchlist_response_json = JSON.parse(add_to_my_watchlist_response.body)
					if add_to_my_watchlist_response_json['status'] == 1
						status = 1
						message = add_to_my_watchlist_response_json['message']
					end
				end
			end

			hydra.queue(add_to_my_watchlist_request)
			hydra.run
		end

		apiresponse = {
			:status => status,
			:message => message
		}

		render :json => apiresponse.to_json
	end
end
