module IndexHelper
	ACTION = 'is_action'
	ROMANCE = 'is_romance'
	THRILLER = 'is_thriller'
	COMEDY = 'is_comedy'
	SCI_FI = 'is_sci_fi'
	CRIME = "is_crime"                    
	FAMILY = "is_family"                   
	MYSTERY = "is_mystery"                   
	HORROR = "is_horror"               
	ANIMATION = "is_animation"                 
	BIOGRAPHY = "is_biography"                 
	ADVENTURE =  "is_adventure"

	def get_genre_as_string_help(of_movie)
		genre_array = []
		if (of_movie[ACTION] ) 
			genre_array << 'Action'
		end
		if (of_movie[ROMANCE] ) 
			genre_array << 'Romance'
		end
		if (of_movie[THRILLER] ) 
			genre_array << 'Thriller'
		end
		if (of_movie[COMEDY]) 
			genre_array << 'Comedy'
		end
		if (of_movie[SCI_FI]) 
			genre_array << 'Sci-fi'
		end
		if (of_movie[CRIME]) 
			genre_array << 'Crime'
		end
		if (of_movie[FAMILY]) 
			genre_array << 'Family'
		end
		if (of_movie[MYSTERY]) 
			genre_array << 'Mystery'
		end
		if (of_movie[HORROR]) 
			genre_array << 'Horror'
		end
		if (of_movie[ANIMATION]) 
			genre_array << 'Animation'
		end
		if (of_movie[BIOGRAPHY]) 
			genre_array << 'Biography'
		end
		if (of_movie[ADVENTURE]) 
			genre_array << 'Adventure'
		end
		return genre_array
	end

	def get_bar_class_name_based_on_rating(rating)
		badge_class_name = ''
		bar_class_name = ''
		percentage = rating * 100/5 
		if percentage <=20 && percentage > 0
			badge_class_name =  'important'
			bar_class_name = 'danger'
		elsif percentage > 64
			badge_class_name =  'success'
			bar_class_name = 'success'
		elsif percentage > 20 && percentage <= 64
			badge_class_name = 'warning'
			bar_class_name = 'warning'
		end

		return bar_class_name
	end

	def get_badge_class_name_based_on_rating(rating)
		badge_class_name = 'default'
		  
		if rating.to_f <= 1.0 && rating.to_f > 0
			badge_class_name = 'danger'
		elsif rating.to_f > 1.0 && rating.to_f <= 3.2
			badge_class_name = 'warning'
		elsif rating.to_f >= 3.2
			badge_class_name =  'success'
		end

		return badge_class_name		
	end

	def date_future? (d)
		release_date = Date.strptime( d , '%Y-%m-%d').to_date
		today = Time.zone.now.to_date
		future = 1
		if release_date <=  today 
  			future = 0 
  		end
  		return future
	end

	def opening_this_week? (date)
		release_date = Date.strptime( date , '%Y-%m-%d').to_date

		# check if release date was in past 3 days
		opening = 0

		if 7.days.from_now > release_date
			opening = 1
		end

  		return opening
	end

	def get_zone_id( zone_array , lat, long)
		if Rails.env.development? && lat == 0 && long == 0
			zone_array.each do |country, zones|
				zones.each do |zone|
					if zone['name'] == "Bay Area"
						return zone
					end 
				end
			end				
		else
			zone_array.each do |country, zones|
				zones.each do |zone|
					if (lat == nil || long == nil )
						return nil
					end

					if lat < zone['north'] && lat > zone['south'] && long > zone['west'] && long < zone['east']
						puts zone
						return zone
					end 
				end
			end
			return nil
		end		
	end


	def get_showtimes( zone_id , movie_id)
		hydra = Typhoeus::Hydra.new
		showtimes_request = Typhoeus::Request.new(SHOWTIME_SERVICE+"showtimes/of/"+zone_id.to_s+"/and/"+ movie_id.to_s, :method =>'get')
		showtimes_request.on_complete do |showtimes_response|
			if showtimes_response.code >=200 && showtimes_response.code < 300
				showtimes_response_json = JSON.parse(showtimes_response.body)
				if showtimes_response_json['status'] == 1
					@showtimes = showtimes_response_json['data']
				end
			else
				@showtimes = []	
			end
			return @showtimes
		end
		hydra.queue(showtimes_request)
		hydra.run
	end
end
