module ApplicationHelper

	def highlight_substring(string, sub_string)
		formatted_html = string.downcase.insert(string.index(sub_string.downcase),"<strong>")
		formatted_html = string.downcase.insert(string.index(sub_string.downcase)+3,"</strong>")
		return formatted_html
	end
end
