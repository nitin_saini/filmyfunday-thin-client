class EmailNotifier < ActionMailer::Base
  default :from => "deborshi.saha@gmail.com", :content_type => 'text/plain'

  # send a signup email to the user, pass in the user object that contains the user's email address
  def signup_email(user)
  	@new_user = user
    mail( :to => @new_user['email'] , :subject => "Welcome to MoviePitcher" )
  end

  def test_email()
    mail( :to => 'deborshi.saha@gmail.com' , :subject => "Test email" )
  end
end