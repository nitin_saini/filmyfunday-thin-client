/*
 *	Data Loader functions
 */

function dataLoader(event, domObj) {
	console.log('Hello');
	if (domObj.getAttribute("load-data-type") === "critic_review") {
		//return false;
		event.preventDefault();

		if ( ( domObj.getAttribute("movie-id") == null ) || 
		     ( domObj.getAttribute("next-page") == null) ) {
			return false;
		} else {
			$('#load_more_critic_review i').addClass('icon-spinner icon-spin');
		}

		$.ajax({
			url: '/critic_review_ratings/'+domObj.getAttribute("movie-id"),
			data: { 'next' : domObj.getAttribute("next-page") },
			method: 'get',
			success: function(responseData, textStatus, jqXHR){ afterLoadingCriticReview(responseData, domObj ); } ,
			dataType: 'json'
		});
	} else if ($(domObj).attr("load-data-type") === "showtimes") {
		event.preventDefault();

		$('#location-city span').text($(domObj).attr('value'));
		$('#showtimes-loading').removeClass('hidden-xs hidden-md hidden-sm hidden-lg');
		$('.showtimes-table').addClass('hidden-xs hidden-md hidden-sm hidden-lg');
		$.ajax({
			url: '/showtimes',
			data: { 'movie_id' : $(domObj).attr("movie-id") , 'zone_id' : $(domObj).attr("zone-id") },
			method: 'get',
			success: function(responseData, textStatus, jqXHR){ afterGettingShowtimes(responseData, domObj ); } ,
			dataType: 'json'
		});
	}
}

function afterLoadingCriticReview(responseData, domObj) {
	$('#load_more_critic_review i').removeClass('icon-spinner icon-spin');
	if (responseData.status == 1) {
		for (var i = responseData.data.reviews.length - 1; i >= 0; i--) {
			temp = document.createElement('div');
			$(temp).addClass('media');

			htmlString = '<a class="pull-left" href="#"><img class="media-object img-rounded" height="50" src="http://res.cloudinary.com/haaztcbpd/image/upload/c_fill,d_no-critic-pic.png,g_face,h_50,w_50/komalnahata.jpg" width="50">'
			htmlString += '</a><div class="media-body  panel-body" style="background-color: white;border: 1px solid #FEDFDF;border-radius: 5px;padding-bottom:0px"><h4 class="media-heading">'+responseData.data.reviews[i]['critic_name']+'<small></small></h4>'
			htmlString +=  '<p style="font-size:14px;letter-spacing:1px">'+responseData.data.reviews[i]['review']+'</div>'
			
			temp.innerHTML = htmlString;

			$('#critic_review')[0].insertBefore(temp, $('#critic_review .row')[0] );
		}
		console.log(responseData.data.next);
		if (responseData.data.next) {
			$(domObj).setAttribute("next-page", ''+responseData.data.next+'');
		} else {			
			$('#load_more_critic_review').hide();
		}
		
	}
}


function afterGettingShowtimes(responseData, domObj) {
	if (responseData.status == 1) {
		var p = responseData.data
		var htmlString = ''
		for (var key in p) {
			htmlString += '<tr><td style="padding:0px"><div class="panel panel-default" style="margin-bottom:0px; border-radius: 0px; border:0px">'
			htmlString += '<div class="panel-heading" style="background-color: #E0F4FD;border-top-right-radius:0px;border-top-left-radius: 0px;"><span class="col-lg-11 col-sm-11 col-md-11">'
			htmlString += key
			htmlString += '</span><span class="label label-info"><i class="icon-phone"></i></span></div>'
			htmlString += '<div class="panel-body">'
			for (var i = 0 ; i < p[key].length ; i++) {
				console.log(p[key][i]['showtime'])
				htmlString += '<span class="col-lg-2 label label-success" style="margin-left:10px;margin-top: 2px;">'
				htmlString += p[key][i]['showtime']
				htmlString += '</span>'
			};
			htmlString += '</div></div></td></tr>'
		}
		$('#showtimes-loading').addClass('hidden-xs hidden-md hidden-sm hidden-lg');
		$('.showtimes-table').removeClass('hidden-xs hidden-md hidden-sm hidden-lg');
		$('.showtimes-table tbody')[0].innerHTML = htmlString;
	}
}

function reviewFormValidator(){
	if ( $('#viewer_review_review_title')[0].value.length > 0 && $('#viewer_review_review')[0].value.length > 0 ) {
		$('#viewer_review_submit_button')[0].removeAttribute("disabled");
	} else {
		$('#viewer_review_submit_button')[0].setAttribute("disabled", "disabled");
	}
	//$('#viewer_review_review_title').value.length
	//$('#viewer_review_review').value.length
}