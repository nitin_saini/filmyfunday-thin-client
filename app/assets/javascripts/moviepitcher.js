

	var movies = [];
	var map = {};
	var loading = 0;


	$(function() {
		
		$('.accordion').on('show', function (e) {
			
			if ($(e.target).prev('.accordion-heading').find('.review-accordion-toggle').length > 0) {
				$(e.target).prev('.accordion-heading').find('.review-accordion-toggle').addClass('review-activeAccordion');
			} else {
				$(e.target).prev('.accordion').find('.accordion-group').find('.accordion-heading').addClass('activeAccordion');
			}
	         	
	         		//$(e.target).prev('.accordion-heading').find('.accordion-toggle-href').prev('div').addClass('activeAccordion');
	    	});
	    
	    	$('.accordion').on('hide', function (e) {
	    		if ($(e.target).prev('.accordion-heading').find('.review-accordion-toggle').length > 0) {
	        		$(this).find('.accordion-toggle').not($(e.target)).removeClass('review-activeAccordion');
	        	} else {
	        		$(this).find('.accordion-toggle').not($(e.target)).removeClass('activeAccordion');
	        	}	//$(this).find('.accordion-toggle-href').not($(e.target)).removeClass('activeAccordion');
	    	});
        });

	function getCookie(cookie_name){
		var cookie_value = document.cookie;

		// multiple document.cookies present
		var c_start = cookie_value.indexOf(" " + cookie_name + "=");
		
		if (c_start == -1) {
			// only single document.cookie present
  			c_start = cookie_value.indexOf(cookie_name + "=");
  		}

		if (c_start == -1) {
			// cookie is not available
  			cookie_value = null;
  		} else {
  			// found the cookie
  			c_start = cookie_value.indexOf("=", c_start) + 1;
  			var c_end = cookie_value.indexOf(";", c_start);
  			if (c_end == -1) {
    				c_end = cookie_value.length;
    			}
  			cookie_value = unescape(cookie_value.substring(c_start,c_end));
  		}
		return cookie_value;
	}

	function setCookie(c_name,value,expire_days){
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + expire_days);
		var c_value=escape(value) + ((expire_days==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
		c_value = getCookie(c_name);
		return c_value;
	}

	function checkView() {
		//alert(sender.class);
		
		var vprefl=getCookie("_vprefl");
		if (vprefl!=null && vprefl!="") {
			// Check the value
			if (vprefl == 1) {
				$('#releasedMovieContainerList').show();
				$('#releasedMovieContainter').hide();
				$('#view_preference').addClass('btn-inverse');

			} else {
				$('#releasedMovieContainerList').hide();
				$('#releasedMovieContainter').show();
				$('#view_preference').removeClass('btn-inverse');
			}
  		}  else {
  			$('#releasedMovieContainter').show();
  			$('#releasedMovieContainerList').hide();
  		}
	}

	function setView(sender) {
		
		var vprefl = getCookie("_vprefl");
		if (vprefl!=null && vprefl!="") {
			// Check the value
			if (vprefl == 1) {
				vprefl = 0;

				setCookie("_vprefl",vprefl,365);
				$('#releasedMovieContainerList').hide();
				$('#releasedMovieContainter').show();
				$(sender).removeClass('btn-inverse');
			} else {
				vprefl = 1;
				$('#releasedMovieContainerList').show();
				$('#releasedMovieContainter').hide();
				$(sender).addClass('btn-inverse');

				setCookie("_vprefl",vprefl,365);
			}
  		} else {
  			vprefl = 1;
  			cv = setCookie("_vprefl",vprefl,365);
  			if (cv == 1) {
  				$('#releasedMovieContainerList').show();
				$('#releasedMovieContainter').hide();
				$(sender).addClass('btn-inverse');
  			}
  		}
	}

	function getCurrentUserInfo(){
		console.log('getCurrentUserInfo');
		FB.api('/me?fields=id,name,first_name,last_name,email,gender', function(response) {
			// Fail login if Facebook doesn't send us email
			if (!response.email) {
				$('.facebook-login-fail-alert p').text('You might not have a valid email. Please update your facebook email with valid email address and try again later.');
			}
			$.post( '/fblogin', { user: { fname : response.first_name, lname : response.last_name, gender : response.gender , email : response.email , fb_id : response.id } },function (rd_j) {
				if ( rd_j.status ) {
					getAppUsers();
				} else {
					$('#facebook-login-fail-alert').removeClass('hidden-xs hidden-sm hidden-md hidden-lg');
					$('#facebook-icon').removeClass('icon-spinner icon-spin');
					$('#facebook-icon').addClass('icon-facebook');
				}
			}, "json");
		});
	}

	function getAppUsers(){
		console.log('getAppUsers');
		var fb_u = [];
		FB.api('/me/friends?fields=installed', function(response) {
			var i=0;
			while(i < response.data.length) {
				if (response.data[i] && response.data[i]['installed']) {
					fb_u.push(response.data[i]['id']);
				}
				i++;
			}
			c_s_fb_con = fb_u.join(",");

			if (fb_u.length > 0) {
				console.log('getAppUsers 2');
				$.post( '/fbconnections', { fb_connection_ids : c_s_fb_con },function (rd_j) {
					if ( rd_j.status ) {
						location.reload();
					}
				}, "json");
			} else {
				location.reload();
			}
		});
	}

	function tryFBlogin(callback, callerObject) {
		$('#facebook-icon').removeClass('icon-facebook');
		$('#facebook-icon').addClass('icon-spinner icon-spin');
		FB.getLoginStatus(function(response) {

			if (response.status === 'connected') {
				callback(true);
				
			} else if (response.status === 'not_authorized') {
				login(callback);
				
			} else {
				login(callback);
			}
		});
	}

	function login(callback) {
		FB.login(function(response) {
			if (response.authResponse) {
				callback(true);

			} else {
				callback(false);
			}
		}, {scope: 'email,publish_actions'});
	}

	function tryPostingOnFacebook (review, callback) {
		tryFBlogin(function(loginSuccess){
			if (loginSuccess) {

				FB.api('/me/feed','post',{ message : review }, function(response){
					if (response.id != null) {
						callback(true,response.id);
					}
				});
			}else {
				callback(false, null);
			}
		});
	}

	function FBLoginComplete(){
		getCurrentUserInfo();
	}

   	function rated(data, textStatus, jqXHR){
   		if (data.status) {
   			alert('Success');
   		}
   	}

	function get_movie_array(json_data) {

		movies_name_array = [];
		map = {};
		$.each(json_data, function (i, state) {
			if (state.name ) {
				map[state.name] = state;
				movies_name_array.push(state.name);
			}
		});
		return (movies_name_array);
	}

	function getMovieSuggestionsOnPageLoad (){
		if (movies.length == 0) {
			$.getJSON( '/movie_suggestion', { query: null },function (returned_data) {
				movies = get_movie_array(returned_data);
				//console.log(movies.length)
			});
		} 		
	}

	function sendAccessToken (accessToken){
		if ( accessToken ) {
			$.postJSON( '/fblogin', { fb_at: accessToken },function (returned_data) {

			});
		} 		
	}

	$(document).ready(function() { 
		
		$(window).scroll(function () {
        	
        		if ($(window).scrollTop() == $(document).height() - $(window).height()) {
           			//sendData();
       			}
   		});

		$('[rel=tooltip].disabled').tooltip();
		
 
		var postReviewFormOptions = { 
        		target:        	null,   // target element(s) to be updated with server response 
        		beforeSubmit:  	beforePostingReview,  // pre-submit callback 
        		success:       	afterPostingReview,  // post-submit callback 
 			url:       		'/newpost',
 			type: 			'post',
 			dataType: 		'json',
 			resetForm: 			true
    		}; 

 		var emailPreferencesOptions = { 
	        	target:        	null,   // target element(s) to be updated with server response 
	        	beforeSubmit:  	beforeSettingEmailPreferences,  // pre-submit callback 
	        	success:       	afterSettingEmailPreferences,  // post-submit callback 
	 		url:       		'/account/email_preference',
	 		type: 			'post',
	 		dataType: 		'json',
	 		resetForm: 		false
    		}; 

 		var passwordResetInformationOptions = { 
	        	target:        		'#statusForSendEmailForResetPassword',   // target element(s) to be updated with server response 
	        	beforeSubmit:  		beforeSendingResetPasswordRequest,  // pre-submit callback 
	        	success:       		afterSendingResetPasswordRequest,  // post-submit callback 
	 		url:       		'/send_password_reset_email',
	 		type: 			'post',
	 		dataType: 		'json',
	 		resetForm: 		false
    		};
    	
    		var resetPasswordOptions = {
	        	target:        		'#statusForResetPassword',   // target element(s) to be updated with server response 
	        	beforeSubmit:  		beforeResetPassword,  // pre-submit callback 
	        	success:       		afterResetPassword,  // post-submit callback 
	 		url:       		'/reset_password',
	 		type: 			'post',
	 		dataType: 		'json',
	 		resetForm: 		false    		
    		};

    		var addCriticOptions = {
			target:        	'#statusForResetPassword',   // target element(s) to be updated with server response 
			beforeSubmit:  	beforeAddingCritic,  // pre-submit callback 
			success:       	afterAddingCritic,  // post-submit callback 
			url:       		'/critic/add',
			type: 			'post',
			dataType: 		'json',
			resetForm: 		true    		
		}

		var criticReviewAndRatingOptions = {
			target:        	'#statusForResetPassword',   // target element(s) to be updated with server response 
			beforeSubmit:  	beforeCriticReviewAndRating,  // pre-submit callback 
			success:       	afterCriticReviewAndRating,  // post-submit callback 
			url:       		'/critic/review_rating',
			type: 			'post',
			dataType: 		'json',
			resetForm: 		true    		
		}

		// bind 'myForm' and provide a simple callback function 
		$('#postReviewForm').ajaxForm(postReviewFormOptions); 
		$('#emailPreferences').ajaxForm(emailPreferencesOptions);
		$('#passwordResetInformation').ajaxForm(passwordResetInformationOptions);
		$('#resetPassword').ajaxForm(resetPasswordOptions);
		$('#addCritic').ajaxForm(addCriticOptions);
		$('#criticReviewAndRating').ajaxForm(criticReviewAndRatingOptions);

		$('.add_movie').click(addMovie);
		$('.remove_movie').click(removeMovie);
		
		$('.writable-star').click(function(event){
			console.log('hi');
			rateMovie(event, this);
		});

		$('.watched_movie').click(watchedMovie);

		$('#signout').click(function(event){
			beforeSignOut(event, $(this));
		});

		$('.loader').click(function(event){
			console.log('Hi');
			dataLoader(event, this);
			console.log('Hi 2');
		});
	}); 
	// Before sign out
	function beforeSignOut(eventObj, domObj) {
	   	$.ajax({
			url: '/signout.json',
			data: null,
			method: 'get',
			success: function(responseData, textStatus, jqXHR){ afterSignOut(responseData); } ,
			dataType: 'json'
		});		
	}

	// After sign out
	function afterSignOut(responseData) {
		if (responseData.status == 1) {
			location.reload();
		}
	}

	// sign up
	function beforeSignUp (formData, jqForm, options) {
		$('#signUpForm button i').addClass('icon-spinner icon-spin');
		$('#signUpForm button').attr("disabled","disabled")

		// Remove existing alerts
		$('.form-failure-alert ').addClass('hidden-xs hidden-sm hidden-md hidden-lg');
		$('.form-success-alert ').addClass('hidden-xs hidden-sm hidden-md hidden-lg');
	    	return true;
	}


	function afterSignUp (responseData, textStatus, jqXHR) {
		$('#sign_up').popover('hide');
		if (responseData.status == 0) {
			// alert message
			$('.form-failure-alert ').removeClass('hidden-xs hidden-sm hidden-md hidden-lg');
			for (var i = responseData.message.length - 1; i >= 0; i--) {
				error_string = '<li>'+responseData.message[i]+ '</li>';
				$('.form-failure-alert ul').html(error_string);
			};
		} else {
			// success alert
			$('.form-success-alert ').removeClass('hidden-xs hidden-sm hidden-md hidden-lg');
		}
	}

	// sign in
	function beforeSignIn (formData, jqForm, options) {
		
		$('#signInForm button i').addClass('icon-spinner icon-spin');
		$('#signInForm button').attr("disabled","disabled");

		// Remove existing alerts
		$('.form-failure-alert ').addClass('hidden-xs hidden-sm hidden-md hidden-lg');
		$('.form-success-alert ').addClass('hidden-xs hidden-sm hidden-md hidden-lg');
	    	return true;
	}

	function afterSignIn (responseData, textStatus, jqXHR) {
		$('#sign_in').popover('hide');
		if (responseData.status == 1) {
			// alert message
			location.reload();
		} else {
			$('.form-failure-alert ').removeClass('hidden-xs hidden-sm hidden-md hidden-lg');
			for (var i = responseData.message.length - 1; i >= 0; i--) {
				error_string = '<li>'+responseData.message[i]+ '</li>';
				$('.form-failure-alert ul').html(error_string);
			};			
		}
	}

	function addMovie() { 
		var movieId = null;
		
		if (movieId == null ) {
			movieId = $(this).attr('movie-id');
		}

		if (movieId == null ) {
			return false;
		}

	   	$.ajax({
			url: '/watchlist/add',
			data: { 'movie_id' : movieId },
			method: 'post',
			success: function(responseData, textStatus, jqXHR){ afterAddingMovieToWatchlist(responseData, $(this)); } ,
			dataType: 'json',
			context: $(this)
		});
	}

	function removeMovie() { 
		var movieId = null;
		movieId = $(this).closest('td').prev().find('.watchlistedMovie').attr('watchlistId');
		
		if (movieId == null ) {
			movieId = $(this).attr('movie-id');
		}

		if (movieId == null ) {
			return false;
		}

	   	$.ajax({
			url: '/watchlist/remove',
			data: { 'movie_id' : movieId },
			method: 'post',
			success: function(responseData, textStatus, jqXHR){ afterRemovingFromWatchlist(responseData, $(this)); } ,
			dataType: 'json',
			context: $(this)
		});
	};

	function watchedMovie() { 
		var movieId = $(this).closest('td').prev().find('.watchlistedMovie').attr('watchlistId'); 
	   	$.ajax({
			url: '/watchlist/watched',
			data: { 'movie_id' : movieId },
			method: 'post',
			success: function(responseData, textStatus, jqXHR){ afterAddingToWatchedList(responseData, $(this)); },
			dataType: 'json',
			context: $(this)
		});
	};

	function printSuccess(domObject, message, movie_id) {
		
		domObject.find('.page-alert .alert-error').hide();

		domObject.find('.page-alert .alert-success span').text('Removed');

		domObject.find('.page-alert .alert-success').show();
	}

	function afterAddingToWatchedList (responseData, domObj) {

		console.log('Implement this');
	}

	function beforeCriticReviewAndRating (formData, jqForm, options) {

		$('#criticReviewAndRating button').text('Adding...');
		$('#criticReviewAndRating button').attr("disabled","disabled");
	    return true;
	}

	function afterCriticReviewAndRating (responseText, statusText, xhr, $form) {
		if (responseText.status) {	
			$('#criticReviewAndRating button').text('Add critic');
			$('#criticReviewAndRating button').removeAttr("disabled");
		} else {
			$('#criticReviewAndRating button').text('Failed');
			$('#criticReviewAndRating button').removeClass("btn-primary");
			$('#criticReviewAndRating button').addClass("btn-danger");
		}

	    return true;
	}

	function beforeAddingCritic (formData, jqForm, options) {

		$('#addCritic button').text('Adding...');
		//$('#criticReviewAndRating select').append('<option>Hello</option>')
		$('#addCritic button').attr("disabled","disabled");
		//$('#sendingEmail').show();
	    return true;
	}

	function afterAddingCritic (responseText, statusText, xhr, $form) {
		if (responseText.status) {
			for (var i = 0; i < responseText.data.length; i++) {
				$('#criticReviewAndRating select').append('<option value="'+responseText.data[i].id+'"">'+responseText.data[i].name+'</option>');
			}
			
			$('#addCritic button').text('Add critic');
			$('#addCritic button').removeAttr("disabled");
		} else {
			$('#addCritic button').text('Failed');
			$('#addCritic button').removeClass("btn-primary");
			$('#addCritic button').addClass("btn-danger");
		}
	    return true;
	}

   	function success(data, textStatus, jqXHR){
   		
   		
   		if ( data.data  && data.data.released && data.data.released.length > 0) {
   			released_columns = $('#releasedMovieContainter').children().length;
   			for (var row = 0; row < released_columns; row++) {
   				offset = row;
   				while( data.data.released[offset] && offset < data.data.released.length){
	   				link = "/m/"+data.data.released[offset].id
	   				poster_url = data.data.released[offset].poster_url.replace("\'","\\'");
	   				name = data.data.released[offset].name.replace("\'","\\'");

	   				html  = '<a href="'+link+'" class="well well-mini thumbnail" style="margin-bottom:10px">'
					html += '<div class="row row-without-left-margin"><ul class="thumbnails thumbnails-no-left-margin" style="height:230px;margin-bottom: 0px;">'
					html += '<li><img  height="180" src="http://res.cloudinary.com/haaztcbpd/image/upload/c_fill,h_180,w_130/'+poster_url+'.jpg" width="130">'
					html += '</li><li style="font-size: 12px">'+name+'</li></ul></div></a>'
	   				$('#released_'+row).append(html);
	   				offset += released_columns;
   				}
   			}
   			loading = 0
   		} else if(data.reason) {
   			$('.loading').fadeOut('slow', function() {
   				loading = 1
   			});
   			//$('.loading').hide();
   		}
   		
   	}

	function beforeResetPassword (formData, jqForm, options) {
		$('#resettingPasswordActivityIndicator').show();
		
		$('#resetPassword button').attr("disabled","disabled");
		//$('#sendingEmail').show();
	    	return true;
	}

	function afterResetPassword (responseText, statusText, xhr, $form) {
		
		$('#resettingPasswordActivityIndicator').hide();
		if (responseText.status == 1) {
			$('#resetPassword button').removeAttr("disabled");
			$('#notification').find('.alert-success').find('span').html(responseText.message+'. Please <a href="/signin">sign in</a>.');
			$('#notification').find('.alert-success').show();
		}
		
	    	return true;
	}

	function beforeSendingResetPasswordRequest (formData, jqForm, options) {
		$('#sendingEmailBusyLogo').show();
		$('#passwordResetInformation button').text('Send Email');
		$('#passwordResetInformation button').attr("disabled","disabled")
	    	return true;
	}

	function afterSendingResetPasswordRequest(responseText, statusText, xhr, $form) {
		
		$('#sendingEmailBusyLogo').hide();

		if (responseText.status == 0) {
			$('#passwordResetInformation button').text('Send Email');
			$('#passwordResetInformation button').removeAttr("disabled");
			$('#notification').find('.alert-error').show();
			$('#notification').find('.alert-error').find('span').text('Unknown error occured.');
		} else {
			$('#passwordResetInformation button').text('Send Email');
			$('#passwordResetInformation button').removeAttr("disabled");
			$('#notification').find('.alert-success').show();
			$('#notification').find('.alert-success').find('span').text('Your password reset information has been emailed.');
		}

		return;
	}

	function beforePostingReview (formData, jqForm, options) {

		var movieName = null;
		var movieReview = null;
		var movieReviewTextForPostingOnFacebook = null;
		var bShouldPostOnFacebook = false;

		$('#postReviewForm button i').addClass('icon-spinner icon-spin');
		$('#postReviewForm button').attr("disabled","disabled");
		
		for (var i = 0; i < formData.length ;  i++) {
			if ( formData[i].name == 'movieName') {
			 	movieName = formData[i].value;
			} else if ( formData[i].name == 'viewer_review[review]'){
				movieReview = formData[i].value;
			} else if (formData[i].name == 'postOnFacebook') {
				bShouldPostOnFacebook = formData[i].value;
			}
		};

		if (bShouldPostOnFacebook) {
			// Should post on Facebook
			// Check if User is logged in through Facebook?
			movieReviewTextForPostingOnFacebook =  "reviewed \'"+movieName+"\' and says:\n\n"+movieReview;

			tryPostingOnFacebook ( movieReviewTextForPostingOnFacebook, function( postedSuccessfully, postId){
				if (postedSuccessfully) {
					return true;
				}
			});
		} 

		return true;
	}

	function afterPostingReview(response, statusText, xhr, $form) {

		$('#postReviewForm button i').removeClass('icon-spinner icon-spin');
		if (response.status) {
			data = response.data;
			image_url = '';
			fb_id = getFacebookId();
			temp = document.createElement('div');
			$(temp).addClass('media');

			if (fb_id.length == 0 ) {
				image_url = 'http://res.cloudinary.com/haaztcbpd/image/upload/c_scale,g_north,w_50/male.jpg'
			} else {
				image_url = 'http://graph.facebook.com/'+fb_id+'/picture'
			}

			html_string = '<a class="pull-left" href="#"><img class="media-object img-rounded" src="'+image_url+'" >';
			html_string +='</a><div class="media-body panel-body" style="background-color: white;border: 1px solid #FEDFDF;border-radius: 5px;padding-bottom:0px"><h4 class="media-heading">'+data.review_title+'<small> by '+getFullName()+'</small></h4>';
			html_string +='<p style="font-size:14px;letter-spacing:1px">'+data.review+'</p>'
			temp.innerHTML = html_string;
			$('#viewer_review .jumbotron').hide();
			$('#writeReviewButton').hide();
			$('#writeReview').modal('hide');
			$('#viewer_review')[0].insertBefore(temp, $('#viewer_review .media')[0]);
		}
		return true;
	}

	function showRequest(formData, jqForm, options) { 
	    	return isLoggedIn();
	} 


	function beforeSettingEmailPreferences(formData, jqForm, options) {
		$('#emailPreferences button').attr("disabled", "disabled");
		return true;
	}

	function afterSettingEmailPreferences(responseText, statusText, xhr, $form) {

		if (responseText.status == 1) {
			$('#emailPreferences button').removeAttr("disabled");
		} else if (responseText.status == 0) {
			$('#emailPreferences button').removeAttr("disabled");
		}
	}

	function movieUpdateOrCreate(responseText, statusText, xhr, $form)  {
		alert ( responseText.status );
	}
	

	// post-submit callback 
	function afterAddingMovieToWatchlist(responseData, statusText, xhr, $form)  { 
		
		if (responseData.status == 1) {
			$('.add_movie span').text('Remove from watchlist'); 
			$('.add_movie i').removeClass('icon-plus-sign');

			// Remove class at last
			$('.add_movie').addClass('btn-danger remove_movie'); 
			$('.remove_movie').removeClass('btn-success add_movie');

			$('.remove_movie i').addClass('icon-remove-sign'); 
			$('.remove_movie').unbind('click');
			$('.remove_movie').click(removeMovie);
		} 
	}

	function afterRemovingFromWatchlist (responseData, domObj) {
		
		if (responseData.status == 1) {
			if ($('.remove_movie').length > 1) {
				$(domObj).addClass('btn-success add_movie');
				$(domObj).text('Add to watchlist');
			} else {
				$('.remove_movie span').text('Add to watchlist');
				$('.remove_movie i').removeClass('icon-remove-sign'); 
				 
				// remove class after 
				$('.remove_movie').addClass('btn-success add_movie');
				$('.remove_movie').removeClass('btn-danger remove_movie');

				$('.add_movie i').addClass('icon-plus-sign');
				$('.add_movie').unbind('click');
				$('.add_movie').click(addMovie); 
			}
		} else {
			alert(responseData.message);
		}
	}

	function rateMovie (eventObj, object) { 
		var movieId = null;
		var value = null;
		
		if (movieId == null ) {
			movieId = $(object).attr('movie-id');
			value = $(object).attr('value');
		}
		//eventObj.stopPropagation();
		console.log(movieId);
		console.log(value);
		if (movieId == null || value == null ) {
			return false;
		}

	   	$.ajax({
			url: '/rating',
			data: { 'movie_id' : movieId, 'rating' : value },
			method: 'post',
			success: function(responseData, textStatus, jqXHR){ afterRatingResponse(responseData, object ); } ,
			dataType: 'json',
			context: $(this)
		});
	}

	function afterRatingResponse(responseData, domObj) {
		console.log('afterRatingResponse');
		if (responseData.status) {
			$(domObj).addClass('read-only');
		}
	}

