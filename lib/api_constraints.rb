class ApiConstraints
  def initialize(options)
    @version = options[:version]
    @default = options[:default]
  end

  def matches?(req)
  	puts req.headers
    @default || req.headers['api_version'].include?("v#{@version}")
  end
end